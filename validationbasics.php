<?php

$email1 = filter_var("joe@example.com", FILTER_VALIDATE_EMAIL);
$email2 = filter_var("not_an_email", FILTER_VALIDATE_EMAIL);

echo "<pre>";
var_dump($email1);
var_dump($email2);
echo "</pre>";

echo "<h2>URL validation</h2>";

$url1 = filter_var("http://google.com", FILTER_VALIDATE_URL);
$url2 = filter_var("some_other_thing", FILTER_VALIDATE_URL);

echo "<pre>";
var_dump($url1);
var_dump($url2);
echo "</pre>";
