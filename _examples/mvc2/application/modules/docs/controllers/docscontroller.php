<?php

namespace sevenkevins;

class docsController extends baseController implements IController
{
	/**
	*
	* Constructor, duh
	*
	*/
	public function __construct()
	{
		parent::__construct();

		// set the template directory
		$this->tpl = new view;
		$this->tpl->setTemplateDir(APP_PATH.'/modules/docs/views');

		// a new config
		$config = config::getInstance();
		$this->view->version = $config->config_values['application']['version'];
	}

	/**
	*
	* The index function displays the login form
	*
	*/
	public function index()
	{
		/*** a view variable ***/
		$this->view->title = 'Seven Kevins Blog';
		$this->view->heading = 'Seven Kevins Blog';

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'docs/index' );

		/*** fetch the template ***/
		$this->content = $this->tpl->fetch( 'index.phtml', $cache_id);
	}

	public function configuration()
        {
                /*** a view variable ***/
                $this->view->title = 'Seven Kevins Configuration';
                $this->view->heading = 'Configuration';

                /*** the cache id is based on the file name ***/
                $cache_id = md5( 'docs/configuration.php' );

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'configuration.phtml', $cache_id);

	}


        public function constants()
        {
                /*** a view variable ***/
                $this->view->title = 'Seven Kevins Constants';
                $this->view->heading = 'Constants';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'constants.phtml' );
        }


        public function controllers()
        {
                /*** a view variable ***/
                $this->view->title = 'Seven Kevins Controllers';
                $this->view->heading = 'Controllers';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'controllers.phtml' );
        }


        public function database()
        {
                /*** a view variable ***/
                $this->view->title = 'Seven Kevins Database';
                $this->view->heading = 'Database';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'database.phtml' );
        }


        public function htaccess()
        {
                /*** a view variable ***/
                $this->view->title = 'Seven Kevins htaccess';
                $this->view->heading = '.htaccess';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'htaccess.phtml' );
        }

        public function rss()
        {
                /*** a view variable ***/
                $this->view->title = 'Seven Kevins RSS';
                $this->view->heading = 'RSS';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'rss.phtml' );
        }

        public function mail()
        {
                /*** a view variable ***/
                $this->view->title = 'Seven Kevins Mail';
                $this->view->heading = 'Mail';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'mail.phtml' );
        }


        public function lang()
        {
                /*** a view variable ***/
                $this->view->title = 'Seven Kevins Language';
                $this->view->heading = 'Language';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'language.phtml' );
        }

        public function logging()
        {
                /*** a view variable ***/
                $this->view->title = 'Seven Kevins Logging';
                $this->view->heading = 'Logging';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'logging.phtml' );
        }

        public function namespaces()
        {
                /*** a view variable ***/
                $this->view->title = 'Seven Kevins Namespaces';
                $this->view->heading = 'Namespaces';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'namespaces.phtml' );
        }

        public function modules()
        {
                /*** a view variable ***/
                $this->view->title = 'Seven Kevins Modules';
                $this->view->heading = 'Modules';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'modules.phtml' );
        }

        public function caching_and_templating()
        {
                /*** a view variable ***/
                $this->view->title = 'Seven Kevins Caching and Templating';
                $this->view->heading = 'Caching and Templating';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'caching_and_templating.phtml' );
        }

        public function uri()
        {
                /*** a view variable ***/
                $this->view->title = 'Seven Kevins URI';
                $this->view->heading = 'URI';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'uri.phtml' );
        }

        public function validation()
        {
                /*** a view variable ***/
                $this->view->title = 'Seven Kevins Validation';
                $this->view->heading = 'Validation';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'validation.phtml' );
        }

        public function view()
        {
                /*** a view variable ***/
                $this->view->title = 'Seven Kevins View';
                $this->view->heading = 'View';

                /*** fetch the template ***/
                $this->content = $this->tpl->fetch( 'view.phtml' );
        }

	public function snorg()
	{
		$this->view->title = 'Seven Kevins View';
		$this->view->heading = 'View';
		$this->content = $this->tpl->fetch( 'snorg.phtml' );
	}

	public function events()
	{
		$this->view->title = 'Seven Kevins Events';
		$this->view->heading = 'View';
		$this->content = $this->tpl->fetch( 'events.phtml' );
	}

	public function assets()
	{
		$this->view->title = 'Seven Kevins Assets';
		$this->view->heading = 'View';
		$this->content = $this->tpl->fetch( 'assets.phtml' );
	}
} // end of class
