<?php
/**
 * File containing the admin controller
 *
 * @package Seven Kevins
 * @copyright Copyright (C) 2009 PHPRO.ORG. All rights reserved.
 *
 */

namespace sevenkevins;

class adminController extends baseController implements IController
{

	public function __construct()
	{
		parent::__construct();
		// a new config
		$config = config::getInstance();
		$this->view->version = $config->config_values['application']['version'];
	}

	public function index()
	{
		/*** a new view instance ***/
		$tpl = new view;

		/*** turn caching on for this page ***/
		$tpl->setCaching(true);

		/*** set the template dir ***/
		$tpl->setTemplateDir(APP_PATH . '/modules/admin/views');

		/*** the include template ***/
		// $tpl->include_tpl = APP_PATH . '/views/admin/index.phtml';

		/*** a view variable ***/
		$this->view->title = 'Admin';

		/*** the cache id is based on the file name ***/
		$cache_id = md5( 'admin/index.phtml' );

		/*** fetch the template ***/
		$this->content = $tpl->fetch( 'index.phtml', $cache_id);
	}
}
