<?php

namespace sevenkevins;

class baseController
{
	protected $breadcrumbs, $view, $content=null;

	public function __construct()
	{
		$this->view = new \sevenkevins\view;

		/*** create the bread crumbs ***/
		$bc = new \sevenkevins\breadcrumbs;
		// $bc->setPointer('->');
		$bc->crumbs();
		$this->view->breadcrumbs = $bc->breadcrumbs;

		// a new menu instance
		$menu = new \sevenkevins\menuReader( APP_PATH . '/modules' );

		$uri = \sevenkevins\uri::getInstance();

		$this->view->menu = $menu;
		// javascript loader
		$module = empty( $uri->fragment(0) ) ? 'index' : $uri->fragment(0);
		// did somebody try to pass a dodgey controller value in the url?
		$path = APP_PATH."/modules/$module/assets/js";
		if( is_dir( $path ) )
		{
			$js_loader = new asset_loader( APP_PATH."/modules/$module/assets/js" );
			$this->view->javascript = $js_loader;
		}
		else
		{
			$this->view->javascript = '';
		}

		// css loader
	       $module = empty( $uri->fragment(0) ) ? 'index' : $uri->fragment(0);
		// did somebody try to pass a dodgey controller value in the url?
		$path = APP_PATH."/modules/$module/assets/css";
		if( is_dir( $path ) )
		{
			$css_loader = new asset_loader( APP_PATH."/modules/$module/assets/css" );
			$this->view->css = $css_loader;
		}       
		else    
		{
			$this->view->css = '';
		}
	}

	public function __destruct()
	{
		if( !is_null( $this->content ) )
		{
			$this->view->content = $this->content;
			$result = $this->view->fetch( APP_PATH.'/layouts/index.phtml' );
			$fc = FrontController::getInstance();
			$fc->setBody($result);
		}
	}
}
