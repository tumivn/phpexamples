<style>
  table {
    font-family: helvetica;
    border-collapse:collapse;
  }
  th {
    background: #ccc;
  }
  td, th {
    border:1px solid #aaa;
    padding: 10px;
  }
</style>

<?php
$mysqli = mysqli_connect("localhost", "root", "sfi@123", "test");
$result = $mysqli->query("SELECT * FROM characters");

if($result){?>
<table>
  <tr>
    <th> ID </th>
    <th> First Name </th>
    <th> Last Name </th>
    <th> Occupation </th>
  </tr>
  <?php while($row = $result->fetch_object()){ ?>
  <tr>
	<td><?php echo $row->id;?></td>
	<td><?php echo $row->firstName;?></td>
	<td><?php echo $row->lastName;?></td>
	<td><?php echo $row->occupation;?></td>
  </tr>  
  <?php } ?>
</table>
<?php } ?>

