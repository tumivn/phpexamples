<?php
include_once 'common.php';
//get id if it exists, if not return to index.php
$id = $_GET['id'];
$recipe = NULL;

if (!$id) {
    redirect('index.php');
} else {
    try {
        $db = new PDO(CONNSTR, DBUSER, DBPASS);
    } catch (PDOException $e) {
        echo 'Could not connect to database!';
        exit();
    }
    try{
        $query = 'SELECT * FROM recipes Where id = :recipe_id';

        $recordSet = $db->prepare($query);

        $recordSet->execute(array('recipe_id' => $id));

        $recipe = $recordSet->fetch();
    }  catch (PDOException $e){
        echo "A database problem has occurred: " . $e->getMessage();
        die();
    }
}


if ($recipe) {
    ?>
    <html>
        <head>
            <title>Recipe: </title>
        </head>
        <body>
            <h3>Recipe detail</h3>
            <p>Name: <b><?php echo $recipe['name']; ?></b></p>
            <p>Chef: <?php echo $recipe['chef']; ?></p>
            <p>Create date: <?php echo date('Y-m-d', strtotime($recipe['created'])); ?></p>
            <p>Create date: <?php $date = new DateTime($recipe['created']);
    echo $date->format('d-m-Y'); ?></p>
            <p>Description: <?php echo $recipe['description']; ?></p>
            <p><?php var_dump($recipe) ?></p>
        </body>
    </html>
    <?php
} else {
    ?>
    <h3>Wrong recipe id</h3>
    <?php
    redirect('index.php');
}
?>