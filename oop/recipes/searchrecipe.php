<?php

include_once 'common.php';

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $categoryId = $_POST['id'];

    $db = new PDO(CONNSTR, DBUSER, DBPASS);
    $recipes = NULL;

    if ($categoryId == -1) {
        //Get all recipes
        $query = $db->query('SELECT * FROM recipes');
        $recipes = $query->fetchAll();

        header('Content-Type: application/json');
    } else {
        //Get recipes by categoryID
        $sql = 'SELECT * FROM Recipes WHERE category_id = :category_id';
        $query = $db->prepare($sql);
        $query->bindValue(':category_id', $categoryId);
        $query->execute();    
        $recipes = $query->fetchAll();
    }

    echo json_encode($recipes, JSON_FORCE_OBJECT);
} else {
    redirect('404.php');
    die();
}
?>
