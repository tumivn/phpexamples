<?php
include_once '../common.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    

    if (isset($_POST['name'])) {
        $name = $_POST['name'];
        //Create new category
        try {
            $db = new PDO(CONNSTR, DBUSER, DBPASS);
            $sql = 'INSERT INTO categories(name) VALUES(:name)';
            $query = $db->prepare($sql);

            $query->execute(array(
                ':name' => $name
            ));
        } catch (PDOException $e) {
            echo 'Could not connect to database';
        }
        
        echo "New recipe id: " . $db->lastInsertId();
    }
}
?>