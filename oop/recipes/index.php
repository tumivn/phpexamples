<?php
include_once 'common.php';
$categories = NULL;
$recipes = NULL;
try{
    $db = new PDO(CONNSTR,DBUSER,DBPASS);
    $recipes = $db->query('SELECT * FROM recipes');
    
    //$result = $recipes->fetch(PDO::FETCH_ASSOC);
    //print_r($result);
    
    while($row = $recipes->fetch()){
        echo $row['name'] . ' by ' . $row['chef'] . ' - <a href="recipe.php?id=' . $row['id'] . '">Details</a><br/>';
    }
    
    $categories = $db->query('SELECT * FROM categories');
    
}catch(PDOException $e){
    echo 'Could not connect to database';
}
?>

<html>
<head>
    <script src="jquery-2.0.2.min.js"></script>
    <style>
        li.category { color:darkblue; margin:5px; cursor:pointer; }
        li.category:hover { background:grey; }
    </style>
    <script>
        $(document).ready(function(){
            $("li.category").click(function(){
                alert($(this).val());
                var id = $(this).val();
                //TODO: Load recipes belong to 
                $.ajax({
                    url: "searchrecipe.php",
                    type:"POST",
                    cache: false,
                    data: {id: id},
                    dataType: "JSON"
                }).done(function( data ) {
                    //$("#results").append(data);
                    alert(data[0].chef);
                });
            });
        });
    </script>
</head>
<body>
    <div>
        <h3>Choose category (category list goes here)</h3>
        <ul>
            <li class="category" value="-1">[All]</li>
            <?php 
                while($row = $categories->fetch()){
                    echo '<li class="category" value="'. $row['id'] . '">' . $row['name'] . '</li>';
                }
            ?>
        </ul>    
    </div>
    <div>
        <h3>Result:</h3>
        
    </div>
    <div>
        <a href="category/create.php">Create new category</a>
    </div>
</body>
</html>
