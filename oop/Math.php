<?php

/**
 * Class Math - this class simply does simple Math operations for demo
 * @author Tumi Le <lehoangdung@live.com>
 * @copyright 2013
 * @license path/to/license.txt MIT
 */
class Math {
    /**
     * @param string $stuff, what you're doing
     * @return string
     */
    function doSomething($stuff){
        return "Doing " . $stuff . "</br>";
    }

    /**
     * Add as many arguments (INTs) as you wish, and this method will calculate the total of all agruments
     * @return total of all argurment (INT)
     */
    function add(){
        //get args count
        $count = func_num_args();
        $sum = 0;
        
        for($i=0;$i<$count;$i++){
            $arg = func_get_arg($i);
            is_int($arg)? $sum += $arg : die("Please input int number only..");
        }
        
        return $sum;
    }
}