<?php

class MyClass {
    function __construct($name){
        $this->name = $name;
        echo $this->name . "<br/>";
    }

    private $name;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function doingSomething(){
        echo $this->name . " is doing something</br>";
    }
}