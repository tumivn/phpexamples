<?php
$friends = array("Watson", "Mycroft", "Sherlock");
var_dump(in_array("Lestrade", $friends)); #result is boolean false

echo "<pre>";

print_r(array_slice($friends, 1,1)); # "Mycroft"
print_r(array_slice($friends, -1, 2)); # "Lestrade"
print_r(array_slice($friends, 0, 3)); # "Watson", "Mycroft", "Sherlock"

echo "</pre>";

$friends = array("Watson", "Mycroft", "Sherlock", "Lestrade");
echo "<h2>Examle 2</h2>";
echo "<pre>";
print_r(array_splice($friends, -1, 1, array("Gregory", "Irene")));
print_r($friends);
print_r(array_splice($friends, 0, 3, "Mrs. Hudson"));
print_r($friends);
echo "</pre>";

echo "<h2>Sorting</h2>";
$person = array("name" => "Sherlock", "job" => "Detective", "age" => "Unknown");
asort($person);
print_r($person);

echo "<br/><h2>Count</h2>";
$arr = array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16);
echo count($arr); # 17

echo "<br/><h2>Summing array</h2>";
$arr = array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16);
echo array_sum($arr); # 136

echo "<br/><h2>Scope</h2>";
$friend = "Watson";
function greet () { 
echo "Hello $friend!";
}
greet();
?>
<h2>$_GET</h2>

<?php if( isset($_GET["name"]) ): ?>

<p>Hello, <?php echo $_GET["name"] ?>!</p>

<?php else: ?>

<form method="GET">
<p> Please write your name below:</p>
<p> <input type="text" name="name" /></p>
<p> <button type="submit"> Submit </button></p>
</form>

<?php endif; ?>

<h2> Cookie </h2>

<?php
setcookie("product", "pens", time() + 60*60*24*30);
setcookie("time", time(), time() + 60*60*24*30);
?>
<p> Some info about pens. </p>
<p><a href="./">&larr; Back</a></p>

<?php
echo "This is line " . __LINE__ . " of file " . __FILE__;
